#include <iostream>
#include "Graph.h"
#include <vector>
#include <string>
#include "repo.h"
#include "functionality.h"
#include "UI.h"
using namespace std;

int main() {
	UI ui;
	ui.displayMenu(cout);
	ui.option(cin, cout);
	return 0;
}