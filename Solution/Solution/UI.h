#pragma once
#include <ostream>
#include <istream>
#include <vector>
#include <string>
#include "repo.h"
#include "functionality.h"
#include <stdlib.h> 

using namespace std;

class UI
{
public:
	void displayMenu(std::ostream& os);
	void init(vector<string>& wordList, vector<vector<string>>& fragWords);
	void option(istream& is, ostream& os);
	void automatic(istream& is, ostream& os, repo<string>& r, vector<vector<string>> fragWords);
	void playerMode(istream& is, ostream& os, repo<string>& r, vector<vector<string>> fragWords);
private:

};

