#include "Graph.h"
#include <iostream>

/*Graph::Graph(int n){
	this->current = 0;
	this->number = n;
	this->array = new adjList[n];
	for (int i = 0; i < n; i++) {
		this->array[i].head = nullptr;
	}
}

void Graph::createGraph(std::vector<std::string>& wordList, int numberLetters){
	for (int i = 0; i < wordList.size(); i++) {
		if (wordList[i].size() == numberLetters)
			this->addVertex(wordList[i]);
	}

}

int Graph::addVertex(std::string value) {
	for (int i = 0; i < this->current; i++) {
		if (this->array[i].head->vortex == value)
			return -1;
	}

	if (this->current == this->number) {
		this->resize();
	}
	Node* newNode = this->newNode(value);
	newNode->index = this->current;
	this->array[this->current].head = newNode;
	this->current++;
	return this->current - 1;
}

void Graph::addEdge(std::pair<std::string, int> src, std::pair<std::string, int> dst){
	bool ok1 = true;
	Node* nodeCurrent1 = this->array[src.second].head;
	while (nodeCurrent1 != nullptr) {
		if (nodeCurrent1->vortex == dst.first) {
			ok1 = false;
			break;
		}
		nodeCurrent1 = nodeCurrent1->next;
	}

	if (ok1 == true) {
		Node* newNode1 = newNode(dst.first);
		newNode1->next = this->array[src.second].head->next;
		newNode1->index = dst.second;
		this->array[src.second].head->next = newNode1;
	}
	
	bool ok2 = true;
	Node* nodeCurrent2 = this->array[dst.second].head;
	while (nodeCurrent2 != nullptr) {
		if (nodeCurrent2->vortex == src.first) {
			ok2 = false;
			break;
		}
		nodeCurrent2 = nodeCurrent2->next;
	}

	if (ok2 == true) {
		Node* newNode2 = newNode(src.first);
		newNode2->next = this->array[dst.second].head->next;
		newNode2->index = src.second;
		this->array[dst.second].head->next = newNode2;
	}

}

void Graph::resize(){
	int newCap = this->number * 2;
	adjList* newAdjList = new adjList[newCap];

	for (int i = 0; i < this->number; i++) {
		newAdjList[i].head = this->array[i].head;
	}
	for (int i = this->number; i < newCap; i++) {
		newAdjList[i].head = nullptr;
	}
	delete[] this->array;
	this->array = newAdjList;
	this->number = newCap;

}

void Graph::displayVerteces(std::ostream& os){
	for (int i = 0; i < this->current; i++) {
		os << this->array[i].head->vortex << "\n";
	}
}

Node* Graph::newNode(std::string value){
	Node* newNode = new Node;
	newNode->vortex = value;
	newNode->next = nullptr;
	return newNode;
}



std::vector<std::string> Graph::shortestPath(std::string src, std::string dst){
	int srcIndex, dstIndex;
	for (int i = 0; i < this->current; i++) {
		if (src == this->array[i].head->vortex)
			srcIndex = i;
		if (dst == this->array[i].head->vortex)
			dstIndex = i;
	}
	
	
	std::queue<int> qu;
	bool* used = new bool[this->current];
	int* shortestDistance = new int[this->current];
	int* previous = new int[this->current];

	for (int i = 0; i < this->current; i++) {
		used[i] = false;
		shortestDistance[i] = -1;
		previous[i] = -1;
	}

	shortestDistance[srcIndex] = 0;

	qu.push(srcIndex);
	while (!qu.empty()) {
		int elem = qu.front();
		used[elem] = true;
		qu.pop();
		Node* current = this->array[elem].head;
		while (current != nullptr) {
			if (used[current->index] == false) {
				qu.push(current->index);
				if (shortestDistance[current->index] == -1) {
					shortestDistance[current->index] = shortestDistance[elem] + 1;
					previous[current->index] = elem;
				}
			}
			if (current->index == dstIndex)
				break;
			current = current->next;
		}
		if (current != nullptr)
			break;
	}

	std::vector<std::string> result;
	
	int p = previous[dstIndex];
	result.push_back(dst);
	if (p == -1) {
		result.pop_back();
	}
	while (p != -1) {
		result.push_back(this->array[p].head->vortex);
		p = previous[p];
	}
	return result;

}
*/



