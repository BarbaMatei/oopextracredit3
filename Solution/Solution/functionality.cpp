#include "functionality.h"
#include <iostream>

void functionality::readWords(std::vector<std::string>& wordList){
	std::ifstream f("words_alpha.txt");
	std::string word;
	int max = 0;
	while (f >> word) {
		wordList.push_back(word);
		//if (word.size() > max)
			//max = word.size();
	}
	//std::cout << max;
}

void functionality::fragmentWords(std::vector<std::string>& wordList, std::vector<std::vector<std::string>>& fragWords) {
	for (int i = 0; i <= 31; i++) {
		fragWords.push_back(std::vector<std::string>());
	}

	for (int i = 0; i < wordList.size(); i++) {
		fragWords[wordList[i].length()].push_back(wordList[i]);
	}
}

bool functionality::almostEqual(std::string val1, std::string val2){
	int count = 0;
	for (int i = 0; i < val1.length(); i++) {
		if (val1[i] != val2[i]) {
			count++;
		}
		if (count == 2)
			return false;
	}
	if(count == 0)
		return false;
	return true;
}

string functionality::getName(istream& is, ostream& os) {
	string firstName, lastName;
	os << "Give first name: ";
	is >> firstName;
	os << "Give last name: ";
	is >> lastName;
	return firstName + lastName;
}

void functionality::randomWords(string& start, string& target, vector<vector<string>> fragWords, int wordLength) {
	/*start = fragWords[wordLength][rand() % fragWords[wordLength].size()];
	target = start;
	while (target == start) {
		target = fragWords[wordLength][rand() % fragWords[wordLength].size()];
	}*/

	random_device rd;
	mt19937 generator(rd());

	uniform_int_distribution<> distr(0, fragWords[wordLength].size() - 1);
	start = fragWords[wordLength][distr(generator)];
	target = fragWords[wordLength][distr(generator)];

}

bool functionality::validate(string val1, string val2, Graph<string>& g){
	if (almostEqual(val1, val2) == false)
		return false;
	else {
		auto search = g.getMap().find(val1);
		if (search != g.getMap().end())
			return true;

		//r.getGraph(val1.length()).getMap().find(val2);
		//cout << &r.getGraph(val1.length()).getMap().find(val2);
		//cout << &r.getGraph(val1.length()).getMap().end();
	}
	return false;
}
