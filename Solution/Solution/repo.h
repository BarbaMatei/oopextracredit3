#pragma once
#include "dictionary.h"
#include "graph.h"
#include <vector>
#include <string>
#include <iostream>

using namespace std;

template <typename T>
class repo
{
public:
	repo();
	void createRepo(vector<vector<string>>& fragmentedWordList, int number);
	Graph<T>& getGraph(int);
	//void displayRepo(int numberLetters);
private:
	vector<pair<dictionary<T>, Graph<T>>> repository;
};

template<typename T>
repo<T>::repo()
{
	for (int i = 1; i <= 35; i++) {
		dictionary<T> dict;
		Graph<T> graph;
		this->repository.push_back(pair<dictionary<T>, Graph<T>>(dict, graph));
	}
}

template<typename T>
void repo<T>::createRepo(vector<vector<string>>& fragmentedWordList, int number) {
	this->repository[number].first.createDictionary(fragmentedWordList[number], number);

	for (auto elem : this->repository[number].first.getDict()) {
		for (int k = 0; k < elem.second.size() - 1; k++) {
			for (int j = k + 1; j < elem.second.size(); j++) {
				this->repository[number].second.addEdge(elem.second[k], elem.second[j]);
			}
		}
	}

	//cout << number << "\n";
	//this->repository[number].second.displayEdges(cout);
	//cout << "\n";
}

template<typename T>
Graph<T>& repo<T>::getGraph(int number)
{
	return this->repository[number].second;
}
