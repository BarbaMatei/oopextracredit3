#pragma once
#include <string>
#include <ostream>
#include <vector>
#include <queue>
#include <unordered_map>

using namespace std;

template <typename T>
class Graph {
	
public:
	void addVertex(T value);
	void addEdge(T, T);
	void displayEdges(ostream& os);

	vector<T> shortestPath(T, T);
	unordered_map<T, vector<T>>& getMap();
private:
	unordered_map<T, vector<T>> adjList;
};

template<typename T>
void Graph<T>::addVertex(T value) {
	if (this->adjList.find(value) == this->adjList.end()) {
		std::vector<T> emptyVector;
		this->adjList[value] = emptyVector;
	}
}

template<typename T>
void Graph<T>::addEdge(T val1, T val2) {
	this->adjList[val1].push_back(val2);
	this->adjList[val2].push_back(val1);

}

template<typename T>
vector<T> Graph<T>::shortestPath(T val1, T val2){
	if (this->adjList.find(val1) == this->adjList.end())
		return vector<T>();

	if (this->adjList.find(val2) == this->adjList.end())
		return vector<T>();

	queue<T> qu;
	unordered_map<T,bool> used;
	unordered_map<T, int> shortestDistance;
	unordered_map<T, T> previous;

	for (auto elem: this->adjList) {
		used[elem.first] = false;
		shortestDistance[elem.first] = -1;
		previous[elem.first] = "";
	}

	shortestDistance[val1] = 0;

	qu.push(val1);
	while (!qu.empty()) {
		T elem = qu.front();
		used[elem] = true;
		qu.pop();

		bool finish = false;
		for (auto it : this->adjList[elem]) {
			if (used[it] == false) {
				qu.push(it);
				if (shortestDistance[it] == -1) {
					shortestDistance[it] = shortestDistance[elem] + 1;
					previous[it] = elem;
				}
				if (it == val2) {
					finish = true;
					break;
				}
			}
			
		}
		if (finish == true)
			break;
	}

	vector<T> result;

	T p = previous[val2];
	result.push_back(val2);
	if (p == "") {
		result.pop_back();
	}
	while (p != "") {
		//result.push_back(this->adjList[p]);
		result.push_back(p);
		p = previous[p];
	}
	return result;
}

template<typename T>
unordered_map<T, vector<T>>& Graph<T>::getMap()
{
	return this->adjList;
}

template<typename T>
void Graph<T>::displayEdges(ostream& os) {
	for (auto elem: this->adjList) {
		os << elem.first << ": ";
		for(auto elem2: elem.second){
			os << elem2 << ", ";
		}
		os << "\n";
	}
}