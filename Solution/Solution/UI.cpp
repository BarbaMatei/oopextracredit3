#define _CRT_SECURE_NO_WARNINGS
#include "UI.h"

void UI::displayMenu(std::ostream& os){
	os << "=============MENU=============\n";
	os << "Possible modes:\n";
	os << "\t1.Automatic mode.\n";
	os << "\t2.Player mode.\n";
	os << "\t3.Analitics mode.\n";
	os << "\t4.Show menu.\n";
	os << "\t5.Exit.\n";
}

void UI::init(vector<string>& wordList, vector<vector<string>>& fragWords) {
	functionality funct;
	funct.readWords(wordList);
	funct.fragmentWords(wordList, fragWords);
}

void UI::option(istream& is, ostream& os) {
	vector<string> wordList;
	vector<vector<string>> fragWordList;
	repo<string> Repo;
	this->init(wordList, fragWordList);

	int option;
	while (true) {
		os << "Give option: ";
		is >> option;
		switch (option)
		{
		case 1:
			automatic(is, os, Repo, fragWordList);
			break;
		case 2:
			playerMode(is, os, Repo, fragWordList);
			break;
		case 3:
			break;
		case 4:
			displayMenu(os);
			break;
		default:
			exit(0);
		}
	}
}

void UI::automatic(istream& is, ostream& os, repo<string>& r, vector<vector<string>> fragWords){
	int wordLength = -1;
	string start = "", target = "";
	while (wordLength == -1) {
		os << "Give a word length(between 1 and 31): ";
		is >> wordLength;
		if (wordLength <= 0 || wordLength > 31) {
			wordLength = -1;
			os << "Try again! Give a valid word length.\n\n";
		}
	}
	while (start == "") {
		os << "Give a starting word with " << wordLength << " letters: ";
		is >> start;
		if (start.length() != wordLength) {
			start = "";
			os << "Try again! Give a valid starting word with the desired length.\n\n";
		}
	}
	while (target == "") {
		os << "Give a target word with " << wordLength << " letters: ";
		is >> target;
		if (target.length() != wordLength) {
			target = "";
			os << "Try again! Give a valid starting word with the desired length.\n\n";
		}
	}
	vector<string> answer;
	if (r.getGraph(wordLength).getMap().size() == 0) {
		r.createRepo(fragWords, wordLength);
	}

	answer = r.getGraph(wordLength).shortestPath(target, start);

	os << "Solution: ";
	for (auto it : answer) {
		os << it << " ";
	}
	os << "\n";
	os << "If you'd like to play again this automatic mode press 1. Otherwise, press 4 to display menu.\n";
}

void UI::playerMode(istream& is, ostream& os, repo<string>& r, vector<vector<string>> fragWords) {
	functionality funct;
	ofstream output;
	string name = funct.getName(is, os);
	string start, target;
	string playerWord;
	vector<string> answers;
	vector<string> result;
	int shortestLength = -1;
	int sequenceLength = 1;
	int wordLength = -1;
	auto time = chrono::system_clock::to_time_t(chrono::system_clock::now());
	string timeString = ctime(&time);

	int hints = 0;

	output.open(name + ".csv", ios_base::app);
	os << "Welcome to player mode!\n";

	
	while (wordLength == -1) {
		os << "Give a word length(between 1 and 31): ";
		is >> wordLength;
		if (wordLength <= 0 || wordLength > 31) {
			wordLength = -1;
			os << "Try again! Give a valid word length.\n\n";
		}
	}
	if (r.getGraph(wordLength).getMap().size() == 0) {
		r.createRepo(fragWords, wordLength);
	}
	
	while (result == vector<string>()) {
		funct.randomWords(start, target, fragWords, wordLength);

		result = r.getGraph(start.length()).shortestPath(start, target);
	}
	shortestLength = result.size();
	os << "\n";
	os <<"Starting word: " << start << "\nTarget word: " << target << "\n";
	
	playerWord = "";
	answers.push_back(start);
	while (playerWord != target) {
		os << "For hint press -1.\nEnter next word: ";
		is >> playerWord;
		if (playerWord == "-1") {
			result = r.getGraph(target.length()).shortestPath(target, answers[answers.size() - 1]);
			os << "Next optimal word is: " << result[1] << ".\n";
			hints++;
		}
		else if (funct.validate(playerWord, answers[sequenceLength - 1], r.getGraph(answers[sequenceLength - 1].length())) == true) {
			answers.push_back(playerWord);
			sequenceLength++;
			os << "Sequance: ";
			for (int i = 0; i < answers.size()-1; i++) {
				os << answers[i] << ", ";
			}
			os << answers[answers.size() - 1] << "\n";
		}
		else {
			os << "Word not valid! Please try again!\n";
			playerWord = answers[sequenceLength - 1];
		}
		
	}
	os << "Congratulions! You won this game.\n";
	os << "Your sequence of words is: ";
	for (int i = 0; i < answers.size() - 1; i++) {
		os << answers[i] << ", ";
	}
	os << answers[answers.size() - 1] << ".\n";
	
	//funct.saveInfo(name, );
	timeString.erase(std::prev(timeString.end()));
	output << timeString << ", " << start << ", " << target << ", " << hints << ", ";
	for (auto it : answers) {
		output << it << ", ";
	}
	output << answers.size() << ", " << shortestLength << "\n";
	displayMenu(os);
}

