#pragma once
#include <map>
#include <string>
#include <vector>
#include <iostream>

using namespace std;

template<typename T>
class dictionary
{
public:
	void createDictionary(vector<T>& wordList, int numberLetters);
	//map<string, vector<pair<string, int>>> getDict();
	map<T, vector<T>> getDict();
private:
	map<T, vector<T>> dict;
};

template<typename T>
void dictionary<T>::createDictionary(vector<T>& wordList, int numberLetters) {
	int count = 0;
	for (int i = 0; i < wordList.size(); i++) {
		if (wordList[i].size() == numberLetters) {
			for (int j = 0; j < wordList[i].size(); j++) {
				string wildCard = wordList[i];
				wildCard[j] = '*';
				this->dict[wildCard].push_back(wordList[i]);
			}
			count++;
		}
	}
	//cout << count;
}

template<typename T>
map<T, vector<T>> dictionary<T>::getDict()
{
	return this->dict;
}