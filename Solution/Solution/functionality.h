#pragma once
#include <vector>
#include <string>
#include <fstream>
#include "repo.h"
#include "Graph.h"
#include <random>
#include <chrono>

using namespace std;

class functionality
{
public:
	void readWords(std::vector<std::string>&);
	void fragmentWords(std::vector<std::string>& wordList, std::vector<std::vector<std::string>>& fragWords);
	bool almostEqual(std::string, std::string);
	string getName(istream& is, ostream& os);
	void randomWords(string&, string&, vector<vector<string>>, int);
	bool validate(string, string, Graph<string>& g);
	//void saveInfo(string name, string start, string target, vector<string> wordsUsed, chrono::time_point<chrono::system_clock> date, int hints, )
private:

};

